# Lab9 -- Security check

I'll be testing website [fantlab.ru](https://fantlab.ru/) for different 'Forgot password' requirements.

### Return a consistent message for both existent and non-existent accounts

| Test step                                        | Result                                                                                                            |
|--------------------------------------------------|-------------------------------------------------------------------------------------------------------------------|
| Open fantlab.ru                                  | Ok                                                                                                                |
| Push `забыли пароль?` in upper left corner       | Ok, page with login/email form appears                                                                            |
| Input non-existent user login                    | Ok                                                                                                                |
| Click `получить пароль` to submit fake login     | Ok, text saying about non-existent user appears - `Не удалось найти адрес почтового ящика для этого пользователя` |
| Push `забыли пароль?` in upper left corner again | Ok, page with login/email form appears                                                                            |
| Input existing user login                        | Ok                                                                                                                |
| Click `получить пароль` to submit real login     | Ok, text saying about email sent appears                                                                          |

![img.png](img.png)
![img_1.png](img_1.png)

So the test case failed, results are different. But since the website is just a very local online forum with no payment and stuff, 
the authors are not concerned, probably.

### The user should confirm the password they set by writing it twice
### Ensure that a secure password policy is in place

| Test step                                                         | Result                                                             |
|-------------------------------------------------------------------|--------------------------------------------------------------------|
| Click the link from password reset email                          | Ok, it contains new generated password and link leads to main page |
| Input login and password in upper left corner form                | Ok, page updates and shows account details                         |
| Click `Мой профиль` and then `Правка профиля`                     | Ok, account page appears                                           |
| Tick `сменить пароль` and observe 2 input fields for new password | Ok, there are 2 fields                                             |
| Try entering simple unsecure passwords                            | Ok                                                                 |
| Verify security message mentions password policy violations       | Ok, text lists rules for password to be secure                     |

![img_2.png](img_2.png)

This case passed, there are 2 fields and password policy is in place. Though policy could be stricter according to modern standards.

### Send the user an email informing them that their password has been reset (do not send the password in the email!)
### Don't automatically log the user in, once they have set their new password

| Test step                                            | Result                                                     |
|------------------------------------------------------|------------------------------------------------------------|
| Open page with account details                       | Ok                                                         |
| Tick `сменить пароль` and enter valid password twice | Ok, page reloads                                           |
| Verify logout                                        | Failed, still logged in and account page opens             |
| Verify email about password reset is send            | Failed, after waiting some time there is no email received |
| Logout manually and try login with new password      | Ok, it is possible to login with new password              |

Finally, this 2 security recommendations are clearly not followed by the website. Still this might not be critical considering 
website's business domain.
